﻿using BowlingPointsCalculator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingTestLibrary
{
    [TestFixture]
    public class BowlingTests
    {
        private Line line;

        [SetUp]
        public void CreateLine() {
            this.line = new Line();
        }

        [Test]
        public void OneThrowIsGoingToThrowCollection() {
            line.Throw(1);
            int biggerThanZeroElements = 0;
            foreach (int points in line.GetThrows()) {
                if (points > 0) {
                    biggerThanZeroElements++;
                }
            }

            Assert.That(biggerThanZeroElements, Is.EqualTo(1));
        }

        [Test]
        public void TwentyOneThrowsIsGoingToThrowCollection() {
            int[] thrws = new int[] { 1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3 };
            line.Throw(thrws);
            int biggerThanZeroElements = 0;
            foreach (int points in line.GetThrows()){
                if (points > 0){
                    biggerThanZeroElements++;
                }
            }
            Assert.That(biggerThanZeroElements, Is.EqualTo(21));
        }

        [Test]
        public void IsNinePlusOneSpare() {
            int[] thrws = new int[] { 9,1,3,4,5,6 };
            line.Throw(thrws);
                        
            Assert.That(line.GetFrames()[0], Is.EqualTo(13));
        }

        [Test]
        public void IsTenInFirstFrameAndThrowStrike() {
            int[] thrws = new int[] { 10, 3, 6, 8, 4, 4, 3 };
            line.Throw(thrws);

            Assert.That(line.GetFrames()[0], Is.EqualTo(19));
        }

        [Test]
        public void IsSixPlusOneRegularRound() {
            int[] thrws = new int[] { 6, 1, 6, 4, 2, 5 };
            line.Throw(thrws);

            Assert.That(line.GetFrames()[0], Is.EqualTo(7));
        }

        [Test]
        public void AllFramesAreStrikes() {
            line.Throw(new int[] { 10,10,10,10,10,10,10,10,10,10,10,10 });

            int[] frames = line.GetFrames();
            foreach (int pts in frames) {
                Assert.That(pts, Is.EqualTo(30));
            }
        }

    }
}
