﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingPointsCalculator
{
    public class Line
    {
        private int[] throws;
        private int[] framePoints;
        private int throwNumber;
        private int activeThrow;

        public Line(){
            this.throwNumber = 0;
            this.framePoints = new int[10];
            activeThrow = 0;
            throws = new int[21];
        }

        public int[] GetFrames() {
            activeThrow = 0;
            for(int i = 0; i < 10; i++)
            {
                //spare
                if (throws[activeThrow] + throws[activeThrow + 1] == 10)
                {
                    framePoints[i] = 10 + throws[activeThrow + 2];
                    activeThrow = activeThrow + 2;
                }
                //strike
                else if (throws[activeThrow] == 10)
                {
                    framePoints[i] = 10 + throws[activeThrow + 1] + throws[activeThrow + 2];
                    activeThrow++;
                }
                else {
                //regular frame
                    framePoints[i] = throws[activeThrow] + throws[activeThrow + 1];
                    activeThrow = activeThrow + 2;
                }
            }
            return framePoints;
        }

        public int[] GetThrows() {
            return throws;
        }

        public void Throw(int points) {
            throws[throwNumber] = points;
            throwNumber++;
        }

        public int GetScore() {
            int total = 0;
            foreach (int pts in framePoints) {
                total += pts;
            }
            return total;
        }

        // for easier testing
        public void Throw(int[] throws) {
            for (int i = 0; i < throws.Length; i++) {
                Throw(throws[i]);
            }
        }
    }
}
