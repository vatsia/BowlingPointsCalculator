﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingPointsCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("BowlingPointsCalculator V. -9001");
            Line l = new Line();
            Console.Write("How many times are you going to throw (min: 10, max: 21)? ");
            int framecount = int.Parse(Console.ReadLine());

            // a little bit of input validation
            while (framecount > 21 || framecount < 10) {
                Console.WriteLine("Enter number between 10 to 21");
                framecount = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < framecount; i++)
            {
                Console.Write(i + ": ");
                int points = int.Parse(Console.ReadLine());
                if (points < 0 || points > 10) {
                    Console.WriteLine("Enter points between 0 and 10");
                    i--;
                }
                else
                {
                    l.Throw(points);
                }
                
            }

            int frame = 1;
            foreach (int luku in l.GetFrames()) {
                Console.WriteLine("Frame " + frame + ": " + luku);
                frame++;
            }
            Console.WriteLine("Total: " + l.GetScore());
        }
    }
}
